FROM openjdk:8

RUN apt-get update
RUN apt-get install -y curl git tmux htop maven

# Set timezone to CST
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /root

RUN curl --insecure -o ./sonarscanner.zip -L https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.1.0.1141-linux.zip
RUN unzip sonarscanner.zip
RUN rm sonarscanner.zip

ENV SONAR_RUNNER_HOME=/root/sonar-scanner-3.1.0.1141-linux
ENV PATH $PATH:/root/sonar-scanner-3.1.0.1141-linux/bin

COPY sonar-scanner.properties ./sonar-scanner-cli-3.1.0.1141-linux/conf/sonar-scanner.properties

# Use bash if you want to run the environment from inside the shell, otherwise use the command that actually runs the underlying stuff
#CMD /bin/bash
#CMD sonar-scanner
ENTRYPOINT ["sonar-scanner","-Dsonar.host.url=http://ec2-18-197-28-191.eu-central-1.compute.amazonaws.com:9000 -Dsonar.login=08e6c65e0b530cd5b69cb4d5de82d19df7c4a6ea"]

